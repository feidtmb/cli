package cli

import (
	"log"
	"os"
	"strings"
)

func (c *Command) parse() *Command {
	// Catalog possible flags defined by this command.
	var (
		flagsByName      = make(map[string]Flag)
		flagsByShortName = make(map[string]Flag)
	)
	for _, f := range c.Flags {
		// Ignore invalid flags.
		if f.Name == "" {
			continue
		}

		flagsByName[f.Name] = f
		if f.ShortName != "" {
			flagsByShortName[f.ShortName] = f
		}
	}

	// Catalog possible subcommands defined by this command.
	subcmdsByName := make(map[string]Command)
	for _, sc := range c.subCmds {
		// Ignore invalid subcommands.
		if sc.Name == "" || (len(sc.subCmds) == 0 && sc.Do == nil) {
			continue
		}
		subcmdsByName[sc.Name] = sc
	}

	// Assign remaining args if this is the root command (otherwise they should already be assigned).
	if !c.isSubcmd {
		c.remainingArgs = newArgs(os.Args)
	}

	// Parse each remaining argument, collecting defined flags and the first subcommand if any.
	// Anything that doesn't match will be considered remaining arguments for use by the Do function (on either this command or the subcommand).
	var (
		subcmd             *Command
		lastParsedFlagName string
		parsedFlags        = make(FlagMap)
		remainingArgs      []string
	)
	// For flags we'll start with the set from the parent command, if any, and add to it (overwriting).
	for k, v := range c.parsedFlags {
		parsedFlags[k] = v
	}
	for {
		arg, ok := c.remainingArgs.Next()
		if !ok {
			break
		}

		if lastParsedFlagName != "" {
			parsedFlags[lastParsedFlagName] = arg
			lastParsedFlagName = ""
			continue
		}

		if subcmd == nil {
			if s, ok := subcmdsByName[arg]; ok {
				subcmd = &s
				continue
			}
		}

		if strings.HasPrefix(arg, "-") {
			m := flagsByShortName
			if strings.HasPrefix(arg, "--") {
				m = flagsByName
			}
			name, value := parseFlag(arg)
			if f, ok := m[name]; ok {
				if value != "" {
					parsedFlags[f.Name] = value
					continue
				}
				lastParsedFlagName = f.Name
				continue
			}
			// The flag isn't recognized. Fall through and let it be treated as a remaining argument.
		}

		remainingArgs = append(remainingArgs, arg)
	}

	// Assign values for omitted flags that have defaults, and check that all required flags are accounted for.
	for _, f := range c.Flags {
		if f.Default != "" {
			if _, ok := parsedFlags[f.Name]; !ok {
				parsedFlags[f.Name] = f.Default
			}
		}

		if f.Required {
			if _, ok := parsedFlags[f.Name]; !ok {
				log.Fatalf("flag %s is required", f.Name)
			}
		}
	}

	if subcmd != nil {
		subcmd.parsedFlags = parsedFlags
		subcmd.remainingArgs = newArgs(remainingArgs)
		return subcmd
	}

	c.parsedFlags = parsedFlags
	c.remainingArgs = newArgs(remainingArgs)
	return nil
}

func parseFlag(raw string) (string, string) {
	trimmed := strings.TrimLeft(raw, "-")
	parts := strings.SplitN(trimmed, "=", 2)
	if len(parts) == 1 {
		return parts[0], ""
	}
	return parts[0], parts[1]
}
