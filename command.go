package cli

import (
	"log"
)

type Command struct {
	Name  string
	Flags []Flag
	Do    func(args Args, flags FlagMap) error

	subCmds       []Command
	isSubcmd      bool
	parsedFlags   FlagMap
	remainingArgs Args
}

func (c *Command) AddSubcommand(subcmds ...Command) {
	for _, sc := range subcmds {
		sc.isSubcmd = true
		c.subCmds = append(c.subCmds, sc)
	}
}

func (c Command) Execute() {
	if subcmd := c.parse(); subcmd != nil {
		subcmd.Execute()
		return
	}

	if c.Do == nil {
		log.Fatalf("%s has no implementation", c.Name)
	}

	if err := c.Do(c.remainingArgs, c.parsedFlags); err != nil {
		log.Fatal(err)
	}
}
