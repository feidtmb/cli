package cli

import (
	"bufio"
	"log"
	"strings"
)

type Args struct {
	s    *bufio.Scanner
	done bool
}

func (a *Args) Next() (string, bool) {
	if a.done || a.s == nil {
		return "", false
	}

	defer func() {
		if r := recover(); r != nil {
			log.Fatalf("panic scanning next argument: %v", r)
		}
	}()

	if a.s.Scan() {
		return a.s.Text(), true
	}
	if err := a.s.Err(); err != nil {
		log.Fatalf("error scanning next argument: %v", err)
	}

	a.done = true
	return "", false
}

func newArgs(ss []string) Args {
	return Args{s: bufio.NewScanner(strings.NewReader(strings.Join(ss, "\n")))}
}
