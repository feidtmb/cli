package main

import (
	"fmt"
	"strings"

	"gitlab.com/feidtmb/cli"
)

func main() {
	say := cli.Command{
		Name: "say",
		Flags: []cli.Flag{
			{Name: "language", Default: "EN", Required: true},
		},
	}

	say.AddSubcommand(cli.Command{
		Name: "hello",
		Flags: []cli.Flag{
			{Name: "to", ShortName: "t"},
			{Name: "from", ShortName: "f"},
		},
		Do: func(args cli.Args, flags cli.FlagMap) error {
			if lang := flags["language"]; lang != "EN" {
				return fmt.Errorf("unsupported language %q", lang)
			}

			var greeting strings.Builder
			greeting.WriteString("Hello")
			if to, ok := flags["to"]; ok {
				greeting.WriteString(" " + to)
			}
			if from, ok := flags["from"]; ok {
				greeting.WriteString(" from " + from)
			}
			greeting.WriteString("!\n")

			return cli.Out(strings.NewReader(greeting.String()))
		},
	})

	say.Execute()
}
