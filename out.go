package cli

import (
	"io"
	"os"
)

// Out outputs the contents of the given reader to stdout.
func Out(r io.Reader) error {
	_, err := io.Copy(os.Stdout, r)
	return err
}
