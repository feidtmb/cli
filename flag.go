package cli

type Flag struct {
	Name      string
	ShortName string
	Default   string
	Required  bool
}

type FlagMap map[string]string
